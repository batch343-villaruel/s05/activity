#1
SELECT customerName FROM customers WHERE country = "Philippines";

#2
SELECT contactLastName, contactFirstName FROM customers WHERE customerName = "La Rochelle Gifts"; 

#3
SELECT productName, MSRP FROM products WHERE productName = "The Titanic";

#4
SELECT firstName, lastName FROM employees WHERE email = "jfirrelli@classicmodelcars.com";

#5
SELECT customerName FROM customers WHERE state IS NULL;

#6
SELECT firstName, lastName, email FROM employees WHERE firstName = "Steve" AND lastName = "Patterson";

#7
SELECT customerName, country, creditLimit FROM customers WHERE country != "USA" AND creditLimit > 3000;

#8
SELECT customerNumber FROM orders WHERE comments LIKE "%DHL%";

#9
SELECT productLine FROM productlines WHERE textDescription LIKE "%state of the art%";

#10
SELECT DISTINCT country FROM customers;

#11
SELECT DISTINCT status FROM orders;

#12
SELECT customerName, country FROM customers WHERE country IN("USA", "France", "Canada");

#13
SELECT employees.firstName, employees.lastName, offices.city
FROM employees
JOIN offices
ON employees.officeCode = offices.officeCode
WHERE offices.city = "Tokyo";

#14
SELECT customers.customerName, customers.country
FROM customers
JOIN employees
ON customers.salesRepEmployeeNumber = employees.employeeNumber
WHERE employees.firstName = "Leslie" AND employees.lastName = "Thompson";

#15
SELECT products.productName, customers.customerName
FROM products
JOIN orderdetails
ON products.productCode = orderdetails.productCode
JOIN orders
ON orderdetails.orderNumber = orders.orderNumber
JOIN customers
ON orders.customerNumber = customers.customerNumber
WHERE customers.customerName = "Baane Mini Imports";

#16
SELECT employees.firstName, employees.lastName, customers.customerName, offices.country
FROM employees
JOIN customers
ON employees.employeeNumber = customers.salesRepEmployeeNumber
JOIN offices
ON employees.officeCode = offices.officeCode
WHERE customers.country = offices.country;

#17
SELECT products.productName, products.quantityInStock
FROM products
JOIN productlines
ON products.productLine = productlines.productline
WHERE productlines.productline = "Planes" AND products.quantityInStock < 1000;

#18
SELECT customerName FROM customers WHERE phone LIKE '%+81%';